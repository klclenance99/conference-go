from .keys import PEXELS_API_KEY, OPEN_WEATHER_API_KEY
import requests


def get_picture(content, city, state):
    url = "https://api.pexels.com/v1/search"
    headers = {"Authorization": PEXELS_API_KEY}
    params = {"query": city + " " + state, "per_page": 1}
    print(params)
    res = requests.get(url, params=params, headers=headers)
    data = res.json()
    try:
        photo_url = data["photos"][0]["src"]["original"]
        content["picture_url"] = photo_url
    except:
        content["picture_url"] = None
    return content


def get_weather(city):
    url = "http://api.openweathermap.org/geo/1.0/direct"
    params = {"q": city, "appid": OPEN_WEATHER_API_KEY}
    res = requests.get(url, params=params)
    data = res.json()
    for city in data:
        if city["country"] == "US":
            (lat, lon) = (city["lat"], city["lon"])
    url = "https://api.openweathermap.org/data/2.5/weather"
    params = {"lat": lat, "lon": lon, "appid": OPEN_WEATHER_API_KEY}
    res = requests.get(url, params=params)
    data = res.json()
    try:
        Kelvin = data["main"]["temp"]
        Fahrenheit = ((Kelvin - 273.15) * (9 / 5)) + 32
        return {
            "temperature": Fahrenheit,
            "description": data["weather"][0]["description"],
        }
    except:
        return {"weather": None}
